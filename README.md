# macos-services

Some custom Mac OS service helpers. 

- Base64 Decode Clipboard.workflow - Decodes a string located in the clipboard
- Base64 Decode Selection.workflow - Decodes a string selected in the current application
- Base64 Encode Clipboard.workflow - Encodes the clipboard to base64 and replaces the clipboard value
- Base64 Encode Selection.workflow - Encodes a selected string and copies it to the clipboard

Actions can be accessed via the top application menu, under services.  

Some apps may show services in the context menu when right clicking.

## Installation

Place files in your `~/Library/Services/` folder.

